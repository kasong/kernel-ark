#!/bin/bash
kernelver=$1 && shift
arch=$1 && shift
rootfs=$1 && shift

output="${rootfs}/lib/modules/${kernelver}/crashkernel.conf"

case $arch in
i386|i686|x86*|s390*)
	ck_cmdline="1G-4G:160M,4G-64G:192M,64G-1T:256M,1T-:512M"
	;;
arm*|aarch*)
	ck_cmdline="2G-:448M"
	;;
powerpc|ppc64*)
	ck_cmdline="2G-4G:384M,4G-16G:512M,16G-64G:1G,64G-128G:2G,128G-:4G"
	;;
*)
	exit 1
	;;
esac

cat > "$output" <<EOF
crashkernel=$ck_cmdline
EOF
